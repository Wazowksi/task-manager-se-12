
package ru.iteco.taskmanager.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.iteco.taskmanager.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FindSession_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "findSession");
    private final static QName _FindSessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "findSessionResponse");
    private final static QName _GetSession_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getSession");
    private final static QName _GetSessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "getSessionResponse");
    private final static QName _PutSession_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "putSession");
    private final static QName _PutSessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "putSessionResponse");
    private final static QName _RemoveSession_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "removeSession");
    private final static QName _RemoveSessionResponse_QNAME = new QName("http://endpoint.api.taskmanager.iteco.ru/", "removeSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.iteco.taskmanager.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindSession }
     * 
     */
    public FindSession createFindSession() {
        return new FindSession();
    }

    /**
     * Create an instance of {@link FindSessionResponse }
     * 
     */
    public FindSessionResponse createFindSessionResponse() {
        return new FindSessionResponse();
    }

    /**
     * Create an instance of {@link GetSession }
     * 
     */
    public GetSession createGetSession() {
        return new GetSession();
    }

    /**
     * Create an instance of {@link GetSessionResponse }
     * 
     */
    public GetSessionResponse createGetSessionResponse() {
        return new GetSessionResponse();
    }

    /**
     * Create an instance of {@link PutSession }
     * 
     */
    public PutSession createPutSession() {
        return new PutSession();
    }

    /**
     * Create an instance of {@link PutSessionResponse }
     * 
     */
    public PutSessionResponse createPutSessionResponse() {
        return new PutSessionResponse();
    }

    /**
     * Create an instance of {@link RemoveSession }
     * 
     */
    public RemoveSession createRemoveSession() {
        return new RemoveSession();
    }

    /**
     * Create an instance of {@link RemoveSessionResponse }
     * 
     */
    public RemoveSessionResponse createRemoveSessionResponse() {
        return new RemoveSessionResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link AbstractEntity }
     * 
     */
    public AbstractEntity createAbstractEntity() {
        return new AbstractEntity();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "findSession")
    public JAXBElement<FindSession> createFindSession(FindSession value) {
        return new JAXBElement<FindSession>(_FindSession_QNAME, FindSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "findSessionResponse")
    public JAXBElement<FindSessionResponse> createFindSessionResponse(FindSessionResponse value) {
        return new JAXBElement<FindSessionResponse>(_FindSessionResponse_QNAME, FindSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getSession")
    public JAXBElement<GetSession> createGetSession(GetSession value) {
        return new JAXBElement<GetSession>(_GetSession_QNAME, GetSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "getSessionResponse")
    public JAXBElement<GetSessionResponse> createGetSessionResponse(GetSessionResponse value) {
        return new JAXBElement<GetSessionResponse>(_GetSessionResponse_QNAME, GetSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PutSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "putSession")
    public JAXBElement<PutSession> createPutSession(PutSession value) {
        return new JAXBElement<PutSession>(_PutSession_QNAME, PutSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PutSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "putSessionResponse")
    public JAXBElement<PutSessionResponse> createPutSessionResponse(PutSessionResponse value) {
        return new JAXBElement<PutSessionResponse>(_PutSessionResponse_QNAME, PutSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "removeSession")
    public JAXBElement<RemoveSession> createRemoveSession(RemoveSession value) {
        return new JAXBElement<RemoveSession>(_RemoveSession_QNAME, RemoveSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "removeSessionResponse")
    public JAXBElement<RemoveSessionResponse> createRemoveSessionResponse(RemoveSessionResponse value) {
        return new JAXBElement<RemoveSessionResponse>(_RemoveSessionResponse_QNAME, RemoveSessionResponse.class, null, value);
    }

}
