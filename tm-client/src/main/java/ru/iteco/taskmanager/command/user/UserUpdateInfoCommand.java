package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class UserUpdateInfoCommand extends AbstractCommand {

    @Override
    public String command() {
	return "user-update";
    }

    @Override
    public String description() {
	return "  -  update user information";
    }

    @Override
    public void execute() throws Exception {
	@NotNull
	final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
	@Nullable
	final Session session = serviceLocator.getSessionService().getSession();
	if (session == null)
	    return;
	@Nullable
	final User user = userEndpoint.findUserById(session, session.getUserId());
	if (user == null)
	    return;

	System.out.print("New login: ");
	@NotNull
	final String login = scanner.nextLine();
	user.setLogin(login);
	userEndpoint.merge(session, user);
	userEndpoint.setCurrent(session, login);
	System.out.println("Done");
    }
}
