package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.HashUtil;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String command() {
	return "user-new-password";
    }

    @Override
    public String description() {
	return "  -  create new password for user";
    }

    @Override
    public void execute() throws Exception {
	@NotNull
	final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
	@Nullable
	final Session session = serviceLocator.getSessionService().getSession();
	if (session == null)
	    return;
	@Nullable
	final User user = userEndpoint.findUserById(session, session.getUserId());
	if (user == null)
	    return;

	System.out.print("New password:");
	@NotNull
	final String newPassword = scanner.nextLine();
	user.setPasswordHash(HashUtil.getHash(newPassword));
	userEndpoint.merge(session, user);
	System.out.println("Done");
    }
}
