package ru.iteco.taskmanager.service;

import java.sql.Connection;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.service.ITaskService;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.repository.TaskRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private final TaskRepository taskRepository;
    @Nullable
    public static Connection connection;

    public TaskService(@NotNull final TaskRepository taskRepository) {
	this.taskRepository = taskRepository;
	connection = DatabaseUtil.connect();
    }

    public void merge(@NotNull final Task task) {
	if (task != null)
	    taskRepository.merge(task);
    }

    public void persist(@NotNull final Task task) {
	if (task != null)
	    taskRepository.persist(task);
    }

    @Nullable
    public Task findById(@NotNull final String id) {
	if (!id.equals(null)) {
	    taskRepository.findById(id);
	}
	return null;
    }

    @Nullable
    public Task findByProjectId(@NotNull final String projectId) {
	if (!projectId.equals(null)) {
	    taskRepository.findByProjectId(projectId);
	}
	return null;
    }

    @Nullable
    public Task findByName(@NotNull final String name) {
	if (!name.equals(null)) {
	    return taskRepository.findByName(name);
	}
	return null;
    }

    @Nullable
    public List<Task> findAll() {
	List<Task> list = taskRepository.findAll();
	if (list.size() > 0)
	    return list;
	return null;
    }

    @Nullable
    public List<Task> findAllByOwnerId(@NotNull final String ownerId) {
	List<Task> list = taskRepository.findAllByOwnerId(ownerId);
	if (list.size() > 0)
	    return list;
	return null;
    }

    @Nullable
    public List<Task> findAllByPartOfName(@NotNull final String partOfName) {
	List<Task> list = taskRepository.findAllByPartOfName(partOfName);
	if (list.size() > 0)
	    return list;
	return null;
    }

    @Nullable
    public List<Task> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	List<Task> list = taskRepository.findAllByPartOfDescription(partOfDescription);
	if (list.size() > 0)
	    return list;
	return null;
    }

    public void remove(@NotNull final String id) {
	if (!id.equals(null)) {
	    taskRepository.remove(id);
	}
    }

    public void removeAll() {
	taskRepository.removeAll();
    }
}
