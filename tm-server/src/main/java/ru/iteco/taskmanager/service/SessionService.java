package ru.iteco.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.service.ISessionService;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.repository.SessionRepository;

public class SessionService extends AbstractService implements ISessionService {

    @NotNull
    private SessionRepository sessionRepository;

    public SessionService() {
	sessionRepository = new SessionRepository();
    }

    public void put(Session session) {
	if (session == null)
	    return;
	sessionRepository.put(session);
    }

    @Nullable
    public Session get(Session session) {
	if (session == null)
	    return null;
	return sessionRepository.get(session);
    }

    @Nullable
    public Session remove(Session session) {
	if (session == null)
	    return null;
	return sessionRepository.remove(session);
    }
}
