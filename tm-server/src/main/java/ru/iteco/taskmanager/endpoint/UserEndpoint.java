package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    @WebMethod
    public void merge(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "user") @NotNull User user) {
	SignatureUtil.validate(session);
	serviceLocator.getUserService().merge(user);
    }

    @WebMethod
    public void persist(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "user") @NotNull User user) {
	SignatureUtil.validate(session);
	serviceLocator.getUserService().persist(user);
    }

    @WebMethod
    @Nullable
    public User findUserById(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "id") @NotNull String id) {
	SignatureUtil.validate(session);
	return serviceLocator.getUserService().findById(id);
    }

    @WebMethod
    @Nullable
    public List<User> findAllUser(@WebParam(name = "session") @Nullable final Session session) {
	SignatureUtil.validate(session);
	return serviceLocator.getUserService().findAll();
    }

    @WebMethod
    @Nullable
    public String getCurrent(@WebParam(name = "session") @Nullable final Session session) {
	SignatureUtil.validate(session);
	return serviceLocator.getUserService().getCurrent();
    }

    @WebMethod
    public void setCurrent(@WebParam(name = "session") @Nullable final Session session,
	    @WebParam(name = "login") @NotNull String login) {
	if (SignatureUtil.validate(session) == null)
	    return;
	serviceLocator.getUserService().setCurrent(login);
    }
}
