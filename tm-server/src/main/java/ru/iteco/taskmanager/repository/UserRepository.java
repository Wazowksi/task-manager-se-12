package ru.iteco.taskmanager.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import ru.iteco.taskmanager.api.repository.IUserRepository;
import ru.iteco.taskmanager.constant.Literas;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.enumerate.RoleType;
import ru.iteco.taskmanager.service.UserService;

@NoArgsConstructor
public final class UserRepository implements IUserRepository {

    @SneakyThrows
    public void merge(@NotNull final User user) {
	@NotNull
	final String query = "UPDATE app_user SET " + Literas.EMAIL + " = ?, " + Literas.FIRST_NAME + " = ?, "
		+ Literas.LAST_NAME + " = ?, " + Literas.LOGIN + " = ?, " + Literas.MIDDLE_NAME + " = ?, "
		+ Literas.PASSWORD_HASH + " = ?, " + Literas.PHONE + " = ?, " + Literas.ROLE_TYPE + " = ?, " + "WHERE "
		+ Literas.ID + " = ?";

	@Nullable
	final Connection connection = UserService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setObject(1, user.getEmail());
	statement.setString(2, user.getFisrtName());
	statement.setString(3, user.getLastName());
	statement.setObject(4, user.getLogin());
	statement.setObject(5, user.getMiddleName());
	statement.setObject(6, user.getPasswordHash());
	statement.setString(7, user.getPhone());
	statement.setString(8, user.getRoleType().getDisplayName());
	statement.setString(9, user.getId());
	statement.executeUpdate();
	statement.close();
    }

    @SneakyThrows
    public void persist(@NotNull final User user) {
	@NotNull
	final String query = "INSERT INTO app_user (" + Literas.ID + ", " + Literas.EMAIL + ", " + Literas.FIRST_NAME
		+ ", " + Literas.LAST_NAME + ", " + Literas.LOGIN + ", " + Literas.MIDDLE_NAME + ", "
		+ Literas.PASSWORD_HASH + ", " + Literas.PHONE + ", " + Literas.ROLE_TYPE
		+ ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	@Nullable
	final Connection connection = UserService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, user.getId());
	statement.setObject(2, user.getEmail());
	statement.setString(3, user.getFisrtName());
	statement.setString(4, user.getLastName());
	statement.setObject(5, user.getLogin());
	statement.setObject(6, user.getMiddleName());
	statement.setObject(7, user.getPasswordHash());
	statement.setString(8, user.getPhone());
	statement.setString(9, user.getRoleType().getDisplayName());

	statement.executeUpdate();
	statement.close();
    }

    @Nullable
    @SneakyThrows
    public User findById(@NotNull final String id) {
	@NotNull
	final String query = "SELECT * FROM app_user WHERE " + Literas.ID + " = ?";

	@Nullable
	final Connection connection = UserService.connection;
	if (connection == null)
	    return null;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, id);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	if (!resultSet.next())
	    return null;
	@Nullable
	final User user = fetch(resultSet);
	if (user != null)
	    return user;
	statement.close();
	return null;
    }

    @Nullable
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
	@NotNull
	final String query = "SELECT * FROM app_user WHERE " + Literas.LOGIN + " = ?";

	@Nullable
	final Connection connection = UserService.connection;
	if (connection == null)
	    return null;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, login);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	if (!resultSet.next())
	    return null;
	@Nullable
	final User user = fetch(resultSet);
	if (user != null)
	    return user;
	statement.close();
	return null;
    }

    @Nullable
    @SneakyThrows
    public List<User> findAll() {
	@NotNull
	final String query = "SELECT * FROM app_user";

	@Nullable
	final Connection connection = UserService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<User> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @Nullable
    @SneakyThrows
    private User fetch(@Nullable final ResultSet row) {
	if (row == null)
	    return null;
	@NotNull
	final User user = new User();

	user.setId(row.getString(Literas.ID));
	user.setEmail(row.getString(Literas.EMAIL));
	user.setFisrtName(row.getString(Literas.FIRST_NAME));
	user.setLastName(row.getString(Literas.LAST_NAME));
	user.setLogin(row.getString(Literas.LOGIN));
	user.setMiddleName(row.getString(Literas.MIDDLE_NAME));
	user.setPasswordHash(row.getString(Literas.PASSWORD_HASH));
	user.setPhone(row.getString(Literas.PHONE));

	@Nullable
	final String role = row.getString(Literas.ROLE_TYPE);
	if (role != null) {
	    switch (role) {
	    case ("ADMIN"):
		user.setRoleType(RoleType.ADMIN);
		break;
	    case ("USER"):
		user.setRoleType(RoleType.USER);
		break;
	    }
	}
	return user;
    }
}
