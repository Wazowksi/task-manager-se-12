package ru.iteco.taskmanager.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import ru.iteco.taskmanager.api.repository.IProjectRepository;
import ru.iteco.taskmanager.constant.Literas;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.enumerate.ReadinessStatus;
import ru.iteco.taskmanager.service.ProjectService;

@NoArgsConstructor
public final class ProjectRepository implements IProjectRepository {

    @SneakyThrows
    public void merge(@NotNull final Project project) {
	@NotNull
	final String query = "UPDATE app_project SET " + Literas.OWNER_ID + " = ?, " + Literas.NAME + " = ?, "
		+ Literas.DESCRIPTION + " = ?, " + Literas.DATE_CREATE + " = ?, " + Literas.DATE_BEGIN + " = ?, "
		+ Literas.DATE_END + " = ?, " + Literas.TYPE + " = ?, " + Literas.READINESS_STATUS + " = ?, " + "WHERE "
		+ Literas.ID + " = ?";

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setObject(1, project.getOwnerId());
	statement.setString(2, project.getName());
	statement.setString(3, project.getDescription());
	statement.setObject(4, project.getDateCreated());
	statement.setObject(5, project.getDateBegin());
	statement.setObject(6, project.getDateEnd());
	statement.setString(7, project.getType());
	statement.setString(8, project.getReadinessStatus().getDisplayName());
	statement.setString(9, project.getId());
	statement.executeUpdate();
	statement.close();
    }

    @SneakyThrows
    public void persist(@NotNull final Project project) {
	@NotNull
	final String query = "INSERT INTO app_project (" + Literas.ID + ", " + Literas.OWNER_ID + ", " + Literas.NAME
		+ ", " + Literas.DESCRIPTION + ", " + Literas.DATE_CREATE + ", " + Literas.DATE_BEGIN + ", "
		+ Literas.DATE_END + ", " + Literas.TYPE + ", " + Literas.READINESS_STATUS
		+ ") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, project.getId());
	statement.setObject(2, project.getOwnerId());
	statement.setString(3, project.getName());
	statement.setString(4, project.getDescription());
	statement.setObject(5, project.getDateCreated());
	statement.setObject(6, project.getDateBegin());
	statement.setObject(7, project.getDateEnd());
	statement.setString(8, project.getType());
	statement.setString(9, project.getReadinessStatus().getDisplayName());
	statement.executeUpdate();
	statement.close();
    }

    @Nullable
    @SneakyThrows
    public Project findById(@NotNull final String id) {
	@NotNull
	final String query = "SELECT * FROM app_project WHERE " + Literas.ID + " = ?";

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return null;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, id);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	if (!resultSet.next())
	    return null;
	@Nullable
	final Project project = fetch(resultSet);
	if (project != null)
	    return project;
	statement.close();
	return null;
    }

    @Nullable
    @SneakyThrows
    public Project findByName(@NotNull final String name) {
	@NotNull
	final String query = "SELECT * FROM app_project WHERE " + Literas.NAME + " = ?";

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return null;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, name);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	if (!resultSet.next())
	    return null;
	@Nullable
	final Project project = fetch(resultSet);
	if (project != null)
	    return project;
	statement.close();
	return null;
    }

    @Nullable
    @SneakyThrows
    public List<Project> findAll() {
	@NotNull
	final String query = "SELECT * FROM app_project";

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<Project> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @Nullable
    @SneakyThrows
    public List<Project> findAllByOwnerId(@NotNull final String ownerId) {
	@NotNull
	final String query = "SELECT * FROM app_project WHERE " + Literas.OWNER_ID + " = ?";

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, ownerId);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<Project> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @Nullable
    @SneakyThrows
    public List<Project> findAllByPartOfName(@NotNull final String partOfName) {
	@NotNull
	final String query = "SELECT * FROM app_project WHERE " + Literas.NAME + " LIKE '%?%'";

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, partOfName);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<Project> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @Nullable
    @SneakyThrows
    public List<Project> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	@NotNull
	final String query = "SELECT * FROM app_project WHERE " + Literas.DESCRIPTION + " LIKE '%?%'";

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return new ArrayList<>();

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.setString(1, partOfDescription);
	@NotNull
	final ResultSet resultSet = statement.executeQuery();
	@NotNull
	final List<Project> result = new ArrayList<>();
	while (resultSet.next()) {
	    result.add(fetch(resultSet));
	}
	statement.close();
	return result;
    }

    @SneakyThrows
    public void remove(@NotNull final String id) {
	@NotNull
	final String query = "DELETE FROM app_project WHERE " + Literas.ID + " = " + id;

	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.executeUpdate();
	statement.close();
    }

    @SneakyThrows
    public void removeAll() {
	@NotNull
	final String query = "DELETE * FROM app_project";
	@Nullable
	final Connection connection = ProjectService.connection;
	if (connection == null)
	    return;

	@NotNull
	final PreparedStatement statement = connection.prepareStatement(query);
	statement.executeUpdate();
	statement.close();
    }

    @Nullable
    @SneakyThrows
    private Project fetch(@Nullable final ResultSet row) {
	if (row == null)
	    return null;
	@NotNull
	final Project project = new Project();

	project.setId(row.getString(Literas.ID));
	project.setOwnerId(row.getString(Literas.OWNER_ID));
	project.setName(row.getString(Literas.NAME));
	project.setDescription(row.getString(Literas.DESCRIPTION));
	project.setDateCreated(row.getString(Literas.DATE_CREATE));
	project.setDateBegin(row.getString(Literas.DATE_BEGIN));
	project.setDateEnd(row.getString(Literas.DATE_END));
	project.setType("Project");

	@Nullable
	final String readinessStatus = row.getString(Literas.READINESS_STATUS);
	if (readinessStatus != null) {
	    switch (readinessStatus) {
	    case ("DURING"):
		project.setReadinessStatus(ReadinessStatus.DURING);
		break;
	    case ("PLANNED"):
		project.setReadinessStatus(ReadinessStatus.PLANNED);
		break;
	    case ("READY"):
		project.setReadinessStatus(ReadinessStatus.READY);
		break;
	    }
	}
	return project;
    }
}
