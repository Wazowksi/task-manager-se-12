package ru.iteco.taskmanager.api.repository;

import java.util.List;

import ru.iteco.taskmanager.entity.User;

public interface IUserRepository {

    void merge(final User user);

    void persist(final User user);

    User findById(final String id);

    User findByLogin(final String login);

    List<User> findAll();
}
