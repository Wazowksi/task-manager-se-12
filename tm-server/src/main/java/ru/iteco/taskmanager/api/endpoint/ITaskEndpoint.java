package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.Task;

@WebService
public interface ITaskEndpoint {

    void taskMerge(@WebParam(name = "session") final Session session, @WebParam(name = "task") final Task task);

    @WebMethod
    void taskPersist(@WebParam(name = "session") final Session session, @WebParam(name = "task") final Task task);

    @WebMethod
    Task findTaskById(@WebParam(name = "session") final Session session, @WebParam(name = "id") String id);

    @WebMethod
    Task findTaskByProjectId(@WebParam(name = "session") final Session session,
	    @WebParam(name = "projectId") String projectId);

    @WebMethod
    Task findTaskByName(@WebParam(name = "session") final Session session,
	    @WebParam(name = "taskName") String taskName);

    @WebMethod
    List<Task> findAllTask(@WebParam(name = "session") final Session session);

    @WebMethod
    List<Task> findAllTaskByOwnerId(@WebParam(name = "session") final Session session,
	    @WebParam(name = "ownerId") String ownerId);

    @WebMethod
    List<Task> findAllTaskByPartOfName(@WebParam(name = "session") final Session session,
	    @WebParam(name = "partOfName") String partOfName);

    @WebMethod
    List<Task> findAllTaskByPartOfDescription(@WebParam(name = "session") final Session session,
	    @WebParam(name = "partOfDescription") String partOfDescription);

    @WebMethod
    void removeTaskById(@WebParam(name = "session") final Session session, @WebParam(name = "id") String id);

    @WebMethod
    void removeAllTask(@WebParam(name = "session") final Session session);
}
