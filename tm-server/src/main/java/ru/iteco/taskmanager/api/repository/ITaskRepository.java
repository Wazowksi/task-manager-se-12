package ru.iteco.taskmanager.api.repository;

import java.util.List;

import ru.iteco.taskmanager.entity.Task;

public interface ITaskRepository {

    void merge(final Task task);

    void persist(final Task task);

    Task findById(final String id);

    Task findByProjectId(final String projectId);

    Task findByName(final String name);

    List<Task> findAll();

    List<Task> findAllByOwnerId(final String ownerId);

    List<Task> findAllByPartOfName(final String partOfName);

    List<Task> findAllByPartOfDescription(final String partOfDescription);

    void remove(final String id);

    void removeAll();
}
