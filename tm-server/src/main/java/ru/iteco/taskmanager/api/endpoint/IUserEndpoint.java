package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.entity.User;

@WebService
public interface IUserEndpoint {

    @WebMethod
    void merge(@WebParam(name = "session") Session session, @WebParam(name = "user") User user);

    @WebMethod
    void persist(@WebParam(name = "session") Session session, @WebParam(name = "user") User user);

    @WebMethod
    User findUserById(@WebParam(name = "session") Session session, @WebParam(name = "id") String id);

    @WebMethod
    List<User> findAllUser(@WebParam(name = "session") final Session session);

    @WebMethod
    String getCurrent(@WebParam(name = "session") Session session);

    @WebMethod
    void setCurrent(@WebParam(name = "session") Session session, @WebParam(name = "login") String login);
}
