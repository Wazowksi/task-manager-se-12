package ru.iteco.taskmanager.api.endpoint;

import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.entity.Session;

@WebService
public interface ISessionEndpoint {

    Session getSession(@WebParam(name = "id") final String id, @WebParam(name = "password") final String password);

    Session findSession(@WebParam(name = "session") final Session session);

    void putSession(@WebParam(name = "session") final Session session);

    Session removeSession(@WebParam(name = "session") final Session session);
}
