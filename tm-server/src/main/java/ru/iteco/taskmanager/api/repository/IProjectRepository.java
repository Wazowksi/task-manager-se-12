package ru.iteco.taskmanager.api.repository;

import java.util.List;

import ru.iteco.taskmanager.entity.Project;

public interface IProjectRepository {

    void merge(final Project project);

    void persist(final Project project);

    Project findById(final String id);

    Project findByName(final String name);

    List<Project> findAll();

    List<Project> findAllByOwnerId(final String ownerId);

    List<Project> findAllByPartOfName(final String partOfName);

    List<Project> findAllByPartOfDescription(final String partOfDescription);

    void remove(final String id);

    void removeAll();
}
