package ru.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.entity.Domain;
import ru.iteco.taskmanager.entity.Session;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    Domain saveData(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain loadData(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain jaxbSaveXml(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain jaxbLoadXml(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain jaxbSaveJson(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain jaxbLoadJson(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain jacksonSaveXml(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain jacksonLoadXml(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain jacksonSaveJson(@WebParam(name = "session") final Session session) throws Exception;

    @WebMethod
    Domain jacksonLoadJson(@WebParam(name = "session") final Session session) throws Exception;
}
