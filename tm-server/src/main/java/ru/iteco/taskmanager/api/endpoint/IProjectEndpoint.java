package ru.iteco.taskmanager.api.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Session;

@WebService
public interface IProjectEndpoint {

    void projectMerge(@WebParam(name = "session") final Session session,
	    @WebParam(name = "project") final Project project);

    @WebMethod
    public void projectPersist(@WebParam(name = "session") final Session session,
	    @WebParam(name = "project") final Project project);

    @WebMethod
    public Project findProjectById(@WebParam(name = "session") final Session session, @WebParam(name = "id") String id);

    @WebMethod
    public Project findProjectByName(@WebParam(name = "session") final Session session,
	    @WebParam(name = "projectName") String projectName);

    @WebMethod
    public List<Project> findAllProject(@WebParam(name = "session") final Session session);

    @WebMethod
    public List<Project> findAllProjectByOwnerId(@WebParam(name = "session") final Session session,
	    @WebParam(name = "ownerId") final String ownerId);

    @WebMethod
    public List<Project> findAllProjectByPartOfName(@WebParam(name = "session") final Session session,
	    @WebParam(name = "partOfName") final String partOfName);

    @WebMethod
    public List<Project> findAllProjectByPartOfDescription(@WebParam(name = "session") final Session session,
	    @WebParam(name = "partOfDescription") final String partOfDescription);

    @WebMethod
    public void removeProjectById(@WebParam(name = "session") final Session session, @WebParam(name = "id") String id);

    @WebMethod
    public void removeAllProject(@WebParam(name = "session") final Session session);
}
